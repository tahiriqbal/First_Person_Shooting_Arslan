﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CnControls;

public enum MouseAxis{
	mousex,mousey

}

public class FPSCamera : MonoBehaviour {

	
	public MouseAxis mouseAxises;
	private float mouseX;
	private float mouseY;

	public float sensitivityX;
	public float sensititvityY;

	public float minX;
	public float maxX;

	public float minY;
	public float maxY;

	Quaternion orginalRotation;

	// Use this for initialization
	void Start () {

		orginalRotation=transform.localRotation;

	}
	
	// Update is called once per frame
	void FixedUpdate () {

			
		if(mouseAxises==MouseAxis.mousex){

			//#if Unity_Android
			mouseX+= CnControls.Touchpad.touchPadX/3*sensitivityX;
					
			//#elif Unity_Editor
			//mouseX += Input.GetAxis ("Mouse X")*sensitivityX;
			//#endif
		}
		if(mouseAxises==MouseAxis.mousey){
			//#if Unity_Android
			mouseY+= CnControls.Touchpad.touchPadY/3*sensititvityY;
			//#elif Unity_Editor
			//mouseY += Input.GetAxis ("Mouse Y")*sensititvityY;
			//#endif
		}



		mouseX = ClampAngle (mouseX, minX, maxX);
		mouseY = ClampAngle (mouseY, minY, maxY);

		Quaternion xAngle = Quaternion.AngleAxis (mouseX, Vector3.up);
		Quaternion yAngle = Quaternion.AngleAxis (mouseY, Vector3.left);

		transform.localRotation = orginalRotation * xAngle * yAngle;


	}

	private float ClampAngle(float angle,float min,float max){

		if (angle < -360) {
		
			angle += 360;
		
		}
		if (angle > 360) {
		
			angle -= 360;
		
		}

		return Mathf.Clamp (angle, min, max);


	}


}
