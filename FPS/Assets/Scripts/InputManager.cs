﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CnControls;

public class InputManager : MonoBehaviour {

	public static float moveX{ get; private set;}
	public static float moveY{get; private set;}
	public static bool fire{ get;  set;}
	public static string switchWeaponKeyPressed;

	public List<GameObject> weaponList = new List<GameObject> ();

	
	// Update is called once per frame
	void Update () {

		moveX = 0;
		moveY = 0;
	
	
		float x=CnControls.SensitiveJoystick.joystickX;
		float y=CnControls.SensitiveJoystick.joystickY;

		#if UNITY_ANDROID
		if(x<0){

			moveX=x;

		}
		if(x>0){
			moveX=x;
		}
		if(y<0){
			moveY=y;
		}
		if(y>0){
			moveY=y;
		}

		#endif

		#if UNITY_EDITOR

		if(Input.GetKey(KeyCode.W)){

			moveY = 1;

		}
		if (Input.GetKey (KeyCode.S)) {

			moveY = -1;

		}

		if (Input.GetKey (KeyCode.A)) {

			moveX = -1;

		}
		if (Input.GetKey (KeyCode.D)) {

			moveX = 1;

		}
		#endif
		if(Input.GetButtonDown("Fire")){

			//fire = true;

		}


	}

	public void fireInput(){

		fire=true;

	}


}
