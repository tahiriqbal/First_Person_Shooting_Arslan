﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testRaycasting : MonoBehaviour {


	public float damage = 10f;
	public float range=100f;
	public GameObject bullet;
	public Camera main;


	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.Space)) {

			fire ();	
		}

	}

	private void fire(){

		print ("fired");

		RaycastHit hitInfo;
		if (Physics.Raycast (main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f)), out hitInfo, range)) {


			Instantiate (bullet, hitInfo.point, Quaternion.LookRotation (hitInfo.normal));

		}

	}
}
