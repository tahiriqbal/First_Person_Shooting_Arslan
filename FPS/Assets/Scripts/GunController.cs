﻿using System.Collections;
using UnityEngine;


public enum Weapontype { Gun, Sniper,Pistol }
public class GunController : MonoBehaviour {

	[Header("Weapon Type")]
	public Weapontype aimMode;

	public int bulletsLeft=0;
	public int bulletsPerMag = 50;
	public int Mag=1;
	public float damage = 10f;
	public float range=100f;

	[Header("AnimationSettings")]
	private Animation weaponAnimation;
	public string fireAnim="Fire";
	public string reloadAnim="reload";
	public string emptyAnim="empty";
	public GameObject bullet;
	[Header("Audio")]
	
	public AudioSource weaponSounds;
	public AudioClip fireSound;
	public AudioClip reloadSound;
	public AudioClip emptySound;
	
	private Camera main;



	

	 

	// Use this for initialization
	void Start () {

		weaponAnimation = gameObject.GetComponent<Animation> ();
		weaponSounds = gameObject.GetComponent<AudioSource> ();
		main = GetComponentInParent<Camera> ();
		bulletsLeft=Mag*bulletsPerMag;
		
	}

	void  OnEnable()
	{
		
	}

	// Update is called once per frame
	void Update () {

		if (InputManager.fire==true ) {
			
			print("called");
			gunFire ();	
		}

		if(Input.GetKeyDown(KeyCode.P)){

			//weaponAnimation.Play("M4_Draw");
			weaponAnimation.Play("SwitchAnim");
		}
		
	}



	private void gunFire(){

		RaycastHit hitInfo;
		if (Physics.Raycast (main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f)), out hitInfo, range)) {

			

			if(bulletsLeft>0){

				weaponAnimation.Play (fireAnim);
				bulletsLeft-=1;

				Instantiate (bullet, hitInfo.point, Quaternion.LookRotation (hitInfo.normal));

				weaponSounds.clip=fireSound;
				weaponSounds.Play();
					
			}
			if(bulletsLeft<=0){
				weaponAnimation.Play(emptyAnim);
				weaponSounds.clip=emptySound;
				weaponSounds.Play();
				Debug.Log("out of bullets");

			}


		}

			InputManager.fire = false;
	}
}
