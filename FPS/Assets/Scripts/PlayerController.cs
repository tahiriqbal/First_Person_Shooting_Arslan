﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerController
{
    public class PlayerController : MonoBehaviour
    {

        private CharacterController characterController;
        public float PlayerMovingSpeed = 5;
        private Vector3 moveDirection;
        public float jumpSpeed = 100.0f;

        // Use this for initialization
        void Start()
        {

            characterController = GetComponent<CharacterController>();
        }

        // Update is called once per frame
        void LateUpdate()
        {

            float x = CnControls.SimpleJoystick.joystickX;
            float y = CnControls.SimpleJoystick.joystickY;

            if (characterController.isGrounded)
            {


                moveDirection = new Vector3(InputManager.moveX * Time.deltaTime * PlayerMovingSpeed, -1, InputManager.moveY * Time.deltaTime * PlayerMovingSpeed);
                moveDirection = transform.TransformDirection(moveDirection);
                characterController.Move(moveDirection);

            }

            if (Input.GetKeyDown(KeyCode.Space))
            {

                moveDirection.y = jumpSpeed;

            }
            moveDirection.y -= 20f * Time.deltaTime;

            characterController.Move(moveDirection);
        }
    }
}
